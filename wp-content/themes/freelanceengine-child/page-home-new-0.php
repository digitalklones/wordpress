<?php
/**
 * Template Name: Homepage New
 */

global $post;
global $user_ID;
if ( is_user_logged_in() ) {
	wp_redirect( home_url() );
	exit;
}

get_header();
// the_post();
// Redirect after login success

$my_projects_url = get_page_link( get_page_by_path('my-project'));

$re_url = '';
if ( isset( $_GET['ae_redirect_url'] ) ) {
	$re_url = $_GET['ae_redirect_url'];
} else {
	//$re_url = home_url();
	$re_url = $my_projects_url;
}

//echo "<p>" . $my_projects_url . "</p>"; 
?>

<!-- Block Banner -->
<div class="fre-background" id="background_banner" style="background-image: url('<?php echo get_theme_mod("background_banner") ? get_theme_mod("background_banner") : get_template_directory_uri()."/img/fre-bg.png";?>');">
	<div class="fre-bg-content">
		<div class="container">
			<h1 id="title_banner"><?php echo get_theme_mod("title_banner") ? get_theme_mod("title_banner") : __("Find perfect freelancers for your projects or Look for freelance jobs online?", ET_DOMAIN);?></h1>
			<?php if(!is_user_logged_in()){ ?>
				<?php if(!fre_check_register()){ ?>
					<a class="fre-btn primary-bg-color" href="<?php echo get_post_type_archive_link( PROFILE ); ?>"><?php _e('Find Freelancers', ET_DOMAIN);?></a>
					<a class="fre-btn primary-bg-color" href="<?php echo get_post_type_archive_link( PROJECT ); ?>"><?php _e('Find Projects', ET_DOMAIN);?></a>
				<?php }else{ ?>
					<?php if(wp_redirect($my_projects_url)){ 
			    		exit;
					} else{
						echo "<p>Can't get your projects. The URL of your projects page can't be found (" . $my_projects_url . ").</p>";
					} ?>
				<?php } ?>
			<?php }else{ ?>
				<div class="fre-page-wrapper">
				    <div class="fre-page-section">
				        <div class="container">
				            <div class="fre-authen-wrapper">
				                <div class="fre-authen-login">
				                	<h2><?php _e( 'Log in', ET_DOMAIN ) ?></h2>
				                	<form role="form" id="signin_form" class="">
				                		<input type="hidden" value="<?php echo $re_url ?>" name="ae_redirect_url"/>
				                        <div class="fre-input-field">
				                            <input type="text" name="user_login"
				                                   placeholder="<?php _e( 'Username or Email', ET_DOMAIN ) ?>">
				                        </div>
				                        <div class="fre-input-field">
				                            <input type="password" name="user_pass"
				                                   placeholder="<?php _e( 'Password', ET_DOMAIN ) ?>">
				                        </div>
				                        <?php //ae_gg_recaptcha( $container = 'fre-input-field' );?>
				                        <div class="fre-input-field">
				                            <button class="btn-submit fre-submit-btn primary-bg-color"><?php _e( 'Log In', ET_DOMAIN ) ?></button>
				                        </div>
				                        <div class="fre-input-field">
				                            <label class="fre-checkbox login-remember" for="remember">
				                                <input id="remember" name="remember" type="checkbox">
				                                <span></span>
												<?php _e( 'Remember me', ET_DOMAIN ) ?>
				                            </label>
				                        </div>
				            		</form>
				            		<div class="fre-login-social">
										<?php
										if ( fre_check_register() && function_exists( 'ae_render_social_button' ) ) {
											$before_string = __( "You can use your social account to log in", ET_DOMAIN );
											ae_render_social_button( array(), array(), $before_string );
										}
										?>
				                    </div>
				                    <div class="fre-authen-footer">
										<?php if ( fre_check_register() ) { ?>
				                            <div class="not-yet-register">
				                                <a href="<?php echo et_get_page_link( "register" ) ?>"
				                                   class=""><?php _e( 'Not yet registered?', ET_DOMAIN ) ?></a>
				                            </div>
										<?php } ?>
				                        <div class="forgot-password">
				                            <a href="<?php echo et_get_page_link( "forgot-password" ) ?>"
				                               class=""><?php _e( 'Forgot password?', ET_DOMAIN ) ?></a>
				                        </div>
				                    </div>
				                </div>
				            </div>
				        </div>
				    </div>
				</div>
			<?php } ?>
		</div>
	</div>
</div>

<!-- List Get Started -->
<?php /* get_footer(); */ ?>

