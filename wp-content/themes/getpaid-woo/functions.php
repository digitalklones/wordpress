<?php
function add_theme_scripts() {
    
    wp_enqueue_style( 'Materialize', get_template_directory_uri() . '/css/materialize.min.css’,array(), ‘0.100.2’, ’all');
}
add_action( 'wp_enqueue_scripts', 'add_theme_scripts' );
?>